# mbxcolors

A menu to quickly pick, paste and organize colors (palettes)

## Dependencies

- jgmenu
- xdotool
- xcolor
- gcolor3
- xdg-utils
- xclip
- notify-send or better notify-send.sh

## Installation

```
git clone https://git.maboxlinux.org/Mabox/mbxcolors
cd mbxcolors
sudo make install

## to uninstall
sudo make uninstall
```

